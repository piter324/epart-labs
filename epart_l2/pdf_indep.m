function pdf = pdf_indep(pts, para)
% Computes probability density function assuming feature independence
% pts  - contains points for which pdf is computed (sample = row)
% para - structure containing parameters:
%	para.labels - class labels
%	para.mu - features' mean values (row per class)
%	para.sig - features' standard deviations (row per class)
% pdf - probability density matrix
%	row count = number of samples in pts
%	column count = number of classes

	% final result matrix
	pdf = zeros(rows(pts), rows(para.mu));
	
	% intermediate (one dimensional) density matrix
	onedpdfs = zeros(rows(pts), columns(para.mu));
	
	% YOUR CODE GOES HERE
	
	% for each class
	for class_index = 1:rows(para.mu)
		% for each feature
		for feature_index = 1:columns(para.mu)
			% fill proper column in onepdfs matrix
			onedpdfs(:,feature_index) = normpdf(pts(:,feature_index), para.mu(class_index, feature_index), para.sig(class_index, feature_index));
		% aggregate onepdfs into one column of pdf matrix - row product for a given column
		pdf(:, class_index) = prod(onedpdfs, 2);
	end
end
