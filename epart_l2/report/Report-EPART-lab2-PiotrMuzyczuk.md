# EPART laboratory assignment #2 report - Optimal Bayes Classification
Author: Piotr Muzyczuk

## Assignment
The goal of the assignment was to recognize which symbol was represented on each of 1824 playing cards described using different feature values in `test.txt` file. 

To achieve this classification, first I needed to implement 3 probability density functions, each one calculating the density using different assumptions:
1. Assuming that the features are independent and each attribute distribution is normal.
1. Assuming that we are dealing with a multi-dimensional normal distribution of the features.
1. Using Parzen window to compute the probability density approximation based on the training set.
To train the classifier, I needed to use the training data from `train.txt` file containing 1824 training samples, the same number as testing set mentioned earlier.

After implementing probability density functions, I needed to perform the actual classification of card suites. 

## Classification

### 1. Removing outliers
It is important to check for outliers in the training set. Their presence can significantly change computed distribution parameters,
which may lead to a lot lower recognition quality.

The first statistics I used to determine whether there's at least 1 outlier in each feature, were simple mean, median, min and max functions.
```matlab
> [mean(train); median(train); min(train); max(train)]

ans =

        4.50000        0.18679        0.01484        0.21045        0.20882       79.65832        1.06037        0.00908
        4.50000        0.18259        0.00015        0.00017        0.00000       -0.00000        0.00000       -0.00000
        1.00000        0.12500        0.00000        0.00000        0.00000       -0.00000       -0.00000       -0.00000
        8.00000        5.12127       25.82285      382.13906      380.85837   145296.77100     1934.12276       16.57025
```
We omit the first column, as that's the class a sample belongs to. We can clearly see that every feature has a potential outlier. Feature 5 almost centrainly has one, given a few orders of magnitude difference between maximum and minimum values.

One way of detecting the obvious outliers, is to get the maximum value along with its sample index.
```matlab
> [mv midx] = max(train)
mv =

        8.00000        5.12127       25.82285      382.13906      380.85837   145296.77100     1934.12276       16.57025

midx =

    77   186   186   186   186   186   186   186
```
We can see that sample 186 has the maximum value for each feature, so this is most likely the outlier.

Before making final decision, let's look at a 2D plot for a few features, where X-axis described a sample index and Y-axis described a feature value at that index. I've included plots for each feature below.
|   |   |
|---|---|
|![feature1 values](images/feature1-outliers.gif)|![feature2 values](images/feature2-outliers.gif)|
|![feature6 values](images/feature6-outliers.gif)|![feature7 values](images/feature7-outliers.gif)|

After zooming in, at least for feature 6 and 7, the outlier was indeed sample no. 186. Plots below show that.
|   |   |
|---|---|
|![feature6 outliers zoomed](images/feature6-outliers-zoom.gif)|![feature7 outliers zoomed](images/feature7-outliers-zoom.gif)|

I further confirmed that by selecting that sample along with adjacent ones - one on each side - with commands: 
```matlab
> midx=186
> train(midx-1:midx+1, :)
```
The result was as follows:
```matlab
ans =

   3.0000e+00   1.8168e-01   9.4070e-06   1.9133e-03   4.8942e-06  -4.5632e-10  -1.3606e-08   1.2678e-10
   3.0000e+00   5.1213e+00   2.5823e+01   3.8214e+02   3.8086e+02   1.4530e+05   1.9341e+03   1.6570e+01
   3.0000e+00   1.8123e-01   1.3918e-05   1.8282e-03   5.8809e-06  -6.0979e-10  -2.1916e-08   2.8178e-12
```
This is enough confirmation, let's remove sample no. 186 from the training set:
```matlab
> size(train)
ans =

   1824      8
> train(midx, :) = [];
> size(train)
ans =

   1823      8
```
After removing the first outlier, the simple statictics used earlier are much more promising:
```matlab
> [mean(train); median(train); min(train); max(train)]

ans =

   4.50082   0.18408   0.00068   0.00095   0.00001  -0.00000   0.00000  -0.00000
   5.00000   0.18258   0.00014   0.00017   0.00000  -0.00000   0.00000  -0.00000
   1.00000   0.12500   0.00000   0.00000   0.00000  -0.00000  -0.00000  -0.00000
   8.00000   0.20639   0.00339   0.00275   0.00010   0.00000   0.00000   0.00000
```
Plots also confirm that:
|   |   |
|---|---|
|![feature1 values after removing first outlier](images/feature-1-outliers-2.gif)|![feature2 values after removing first outlier](images/feature-2-outliers-2.gif)|
|![feature6 values after removing first outlier](images/feature-6-outliers-2.gif)|![feature7 values after removing first outlier](images/feature-7-outliers-2.gif)|

Minimum value for feature 1 seems a bit off the limits. Let's check it:
```matlab
> [mv index] = min(train)
mv =

   1.00000   0.12500   0.00000   0.00000   0.00000  -0.00000  -0.00000  -0.00000

index =

    58   641   641   641   641   392    25   538
```
Sample 641 has the minimum value for a few features. I think, treating it as the second outlier and removing it might help for feature 1 while should not cause any problem for other features.
```matlab
> midx = 641
> train(midx, :) = [];
> size(train)
ans =

   1822      8
```
After removing the second outlier, the simple statictics look better, with all values more consistent:
```matlab
> [mean(train); median(train); min(train); max(train)]

ans =

   4.5016e+00   1.8412e-01   6.8258e-04   9.4867e-04   1.4729e-05  -6.9796e-10   2.9713e-07  -9.4668e-11
   5.0000e+00   1.8259e-01   1.4785e-04   1.7434e-04   1.9996e-06  -9.0827e-11   1.3626e-10  -1.8742e-14
   1.0000e+00   1.6918e-01   1.2901e-08   3.2708e-10   2.3348e-11  -5.4538e-09  -1.2140e-07  -3.2642e-09
   8.0000e+00   2.0639e-01   3.3878e-03   2.7514e-03   1.0314e-04   3.7420e-10   3.2304e-06   2.4436e-09
```
The same goes for plots:
|   |   |
|---|---|
|![feature1 values after removing second outlier](images/feature-1-outliers-3.gif)|![feature2 values after removing second outlier](images/feature-2-outliers-3.gif)|
|![feature6 values after removing second outlier](images/feature-6-outliers-3.gif)|![feature7 values after removing second outlier](images/feature-7-outliers-3.gif)|

Indexed of samples holding minimum and maximum values do not repeat, unlike in previous results and plots do not reveal more outliers. I think there are no more outliers to remove.
```matlab
> [mv index] = min(train)
index =

      58    494    927   1642    807    392     25    538
> [mv index] = max(train)
index =

      77    361    873   108     398    854     687   78
```

### 2. Selecting two features and build Bayes classifiers based on them
Then I needed to choose two features for classification that give relatively well separated classes. I've plotted all feature combinations using a loop:
```matlab
for outer=2:8
    for inner=2:8
        if (outer!=inner)
            plot2features(train, outer, inner)
            title(["Feature " num2str(outer-1) " and " num2str(inner-1)])
            print(["feature-class-" num2str(outer-1) "-" num2str(inner-1) ".gif"])
        endif
    endfor
endfor
```

After inspecting the plots, I narrowed it down to 2 combinations: 1,3 and 2,3. I chose combination 1,3 since values are more concise there.
|CHOSEN COMBINATION|NOT CHOSEN|
|---|---|
|![feature 1,3](images/feature-class/feature-class-1-3.gif)|![feature 2,3](images/feature-class/feature-class-2-3.gif)|

I've reduces both sets to those features (feature 1 is column 2 and feature 3 is column 4):
```matlab
> train = train(:, [1 2 4]);
> test = test(:, [1 2 4]);
```

Then, I ran previously implemented functions to prepare parameters and use them to calculate probability density function.
```matlab
> pdfindep_para = para_indep(train);
> pdfmulti_para = para_multi(train);
> pdfparzen_para = para_parzen(train, 0.001);

> base_ercf = zeros(1,3);
> base_ercf(1) = mean(bayescls(test(:,2:end), @pdf_indep, pdfindep_para) != test(:,1));
> base_ercf(2) = mean(bayescls(test(:,2:end), @pdf_multi, pdfmulti_para) != test(:,1));
> base_ercf(3) = mean(bayescls(test(:,2:end), @pdf_parzen, pdfparzen_para) != test(:,1));
> base_ercf
base_ercf =

   0.0263158   0.0049342   0.0241228
```
The error coefficient was lowest for the second classifier (multi-dimensional normal distribution), followed by the third one (Parzer window) and then by the first one (independent features). It was still pretty low (~0.4% - ~2%), which means that training data were well-prepared.

The width of Parzen window was set to `0.001`.

### 3. Reducing the training set to see how it affects classifiers
I have written a function to reduce the number of training samples by randomly picking some percent of samples of each class. I then tested how limiting the number of training samples affects the performance of all 3 classifiers. Since samples are picked randomly, for each percentage I performed 10 experiments and calculated mean value and standard deviation of error coefficients. 

For 10% of the whole training set I got below results. First row represents mean value and the second represents standard deviation. Each columns holds values for different classifiers - independent features, multi-dimensional normal distribution, Parzen window - the same as for values in the previous step.
```matlab
0.0307018   0.0101425   0.0997807
0.0069684   0.0050595   0.0201852
```

For 25% of the training set I got these results:
```matlab
0.0290022   0.0071820   0.0531250
0.0033042   0.0021226   0.0035290
```

Finally, for 50% I got:
```matlab
0.0270833   0.0059759   0.0354715
0.0019546   0.0015816   0.0045508
```

The conclusion drawn from above values is that the more samples we use to train our classifier, the better results we achieve.

### 4. Changing the Parzen window width
I have performed experiments with the classifier that uses Parzen window by changing the window width. A priori probability was 0.125. There were 5 width values to test.
```matlab
parzen_widths = [0.0001, 0.0005, 0.001, 0.005, 0.01];
```
Outcome was computed and stored as below:
```matlab
parzen_res = zeros(1, columns(parzen_widths));

for width_ind = 1:columns(parzen_widths)
	pdfparzen_para = para_parzen(train, parzen_widths(width_ind));
	parzen_res(width_ind) = mean(bayescls(test(:,2:end), @pdf_parzen, pdfparzen_para) != test(:,1));
endfor
```
The base error coefficient values for given window widths are shown below. First row represents Parzen window width and second row represents error coefficient for that width.
```matlab
> [parzen_widths; parzen_res]
ans =

   0.00010000   0.00050000   0.00100000   0.00500000   0.01000000
   0.02850877   0.01699561   0.02412281   0.07949561   0.13925439
```
![Error coefficients for Parzen window widths](images/parzen_window_errorcoef.gif)
The lowest error coefficient was achieved for the window width of `0.0005`.

### 5. Changing a priori probability
If I change a priori probability for black suits to be two times higher, which means there will be two times more black suites in test set, the classification results come out like below. First row represents mean value for 10 experiments, while seconds represents standard deviation. Each column holds results for different classifiers.
```matlab
0.0203216   0.0036550   0.0288012
0.0031545   0.0014208   0.0011005
```
For reference, below are error coefficient values from point 2, where a priori probability was equal for all classes.
```matlab
0.0263158   0.0049342   0.0241228
```
We can see that the error coefficient with greater a priori probabilities for black suits was larger only for Parzen window but not significantly larger (2.88% > 2.41%) and smaller for other classifiers.

### 6. Using 1-NN classifier
The difference in standard deviation between features is not very big, thus data normalization might not be necessary for using 1-NN classifier.
```matlab
> std(train(:,2:end))
ans =

   0.00884455   0.00095129
> std(test(:,2:end))
ans =

   0.00895140   0.00095324
```
Classification based on 1-NN has about 98.2% efficiency, which is pretty good. Here, we take advantage of a fact that chosen features give quite well separated classes, which is favorable for this classifier.
```matlab
pred_labels = []
for test_ind = 1:rows(test)
	pred_labels(test_ind) = (cls1nn(test(test_ind,2:end), train) == test(test_ind,1));
endfor
mean(pred_labels)
```