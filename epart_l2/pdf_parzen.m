function pdf = pdf_parzen(pts, para)
% Approximates probability density function with Parzen window
% pts  - contains points for which pdf is computed (sample = row)
% para - structure containing parameters:
%	para.labels - class labels
%	para.samples - cell array containing class samples
%	para.parzenw - Parzen window width
% pdf - probability density matrix
%	row count = number of samples in pts
%	column count = number of classes

	% final result matrix
	pdf = rand(rows(pts), rows(para.labels));

	% YOUR CODE GOES HERE
	
	% for each class
	for class_index = 1:rows(para.labels)
		% you know number of samples in this class so you can allocate 
		% intermediate matrix (it contains columns f1 f2 ... fn from diagram in instruction)
		onedpdfs = zeros(rows(para.samples{class_index}), columns(para.samples{class_index}));
		
		% for each sample in pts
		for sample_index = 1:rows(pts)
			% for each feature
			for feature_index = 1:columns(para.samples{class_index})
				% fill proper column in onedpdfs with call to normpdf
				% onedpdfs(:,feature_index) = normpdf(pts(:,feature_index), para.mu(class_index, feature_index), para.sig(class_index, feature_index));
				onedpdfs(:,feature_index) = normpdf(para.samples{class_index}(:,feature_index), pts(sample_index, feature_index), para.parzenw/sqrt(rows(para.samples{class_index})));
			end
			% aggregate onedpdfs into a scalar value
			colprod = prod(onedpdfs, 2);
			% and store it in proper element of pdf
			pdf(sample_index, class_index) = mean(colprod);
		end
	end
end