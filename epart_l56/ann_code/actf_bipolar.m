function res = actf_bipolar(tact)
% sigmoid activation function
% tact - total activation 

	% res = tact;
	% linear function is not the best solution
	
	% unipolar activation function
	% res = 1./(1+exp(-tact));
	% bipolar activation function
	res = 2./(1+exp(-tact))-1;
