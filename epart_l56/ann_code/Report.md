# EPART laboratory report
## Sessions #5 & #6: Fashion items classification with neural networks

Author: Piotr Muzyczuk

## Intro
In this assignment, my task was to classify fashion items from the dataset shared by Zalando Research in MNIST format. The classification has to be done using an artificial neural network. The dataset contained images of 10 types of clothing to classify: T-shirt/top, Trouser, Pullover, Dress, Coat, Sandal, Shirt, Sneaker, Bag, Ankle boot.
The set was split into training data (60 000 samples = 6000 of each class * 10 classes) and testing data (10 000 samples = 1000 of each class * 10 classes).

### Back-propagation algorithm
Back-propagation algorithm was implemented to train the network. This algorithm is used to modify input weights in the network based on the calculated error - the difference between the current classification outcome in an iteration (epoch) and expected outcome. Output neuron for the correct class should have a value of 1 while other neurons should have a value of -1.

Training the network relies on computing an error function gradient. Then with each classified sample, weights are adjusted following back the gradient. The weight change can be expressed in an equation below:

![](weight_mod.png)

Where `W` represents all weights collection, while `η` is a learning rate that affects the learning speed.

## Implementing back-propagation algorithm
The first part of the assignment was to implement the algorithm described above and achieve better results than in the reference sample, shown below:
![Reference network outcome](reference_network_outcome.png)
Input weights of a neural network need to be initialized with some randomly generated values. Mine were saved in `rndstate.txt` file to allow for reproducing all the experiments described in this report and also to be able to compare different approaches and modification to the neural network. 

The heart of back-propagation algorithm is the activation function. First, I tried implementing a unipolar activation function that assigns values in range [0, 1].

For **all** experiments below I used 1 hidden layer with 100 neurons in it and the learning rate of 0.001, apart from experiments where I explicitly state otherwise.

## Testing the algorithm on a very small dataset
To make sure the algorithm worked, I tested it on a very small dataset of 20 samples and only 2 classes. After ~250 epochs the network could make 100% good classification, so I moved on to working on actual dataset.

## Unipolar activation function with whole dataset
Then I tried training the network on a whole dataset using above-mentioned unipolar function.

![](unipolar_function.png)

I was able to achieve better results than reference solution after 238 epochs, which took a significant amount of time to compute (~3 hours). I decided to run this neural network for 400 epochs. This took around 6h. The results are presented below.

| Epoch no. | Epoch computation time | OK | Error |
| -- | -- | -- | -- |
| 238 | 30.926363945007324 | 0.10353333333333334 | 0.12570000000000001 |
| ... | | | |
| 390 | 35.404134035110474 | 0.08795 | 0.12039999999999999 |
| 391 | 35.324090003967285 | 0.087866666666666662 | 0.1205 |
| 392 | 35.258960008621216 | 0.087800000000000003 | 0.1205 |
| 393 | 35.497217178344727 | 0.087666666666666671 | 0.12039999999999999 |
| 394 | 35.305338859558105 | 0.087616666666666662 | 0.1203 |
| 395 | 35.277723073959351 | 0.087583333333333332 | 0.1201 |
| 396 | 35.368426084518433 | 0.087499999999999994 | 0.12 |
| 397 | 34.721801042556763 | 0.08741666666666667 | 0.11990000000000001 |
| 398 | 35.288166999816895 | 0.087333333333333332 | 0.11990000000000001 |
| 399 | 35.461331844329834 | 0.0872 | 0.11990000000000001 |
| 400 | 35.249261140823364 | 0.08716666666666667 | 0.11990000000000001 |

The plot and confusion matrix were saved after 400th epoch.

![](rep_unipolar_h100_e400_lr0001.png)

```matlab
cfmx =

   854     0    17    33     3     2    80     0    11     0     0
     5   962     2    23     2     0     5     0     1     0     0
    19     0   802    15   100     0    60     0     4     0     0
    25    10    12   891    35     0    23     0     4     0     0
     0     0    92    36   818     0    51     0     3     0     0
     0     0     0     1     0   944     0    35     2    18     0
   137     1    88    28    76     0   655     0    15     0     0
     0     0     0     0     0    24     0   947     0    29     0
     1     1     3     7     6     2     6     4   970     0     0
     0     0     0     0     0     6     0    35     1   958     0
```

Performance wasn't very impressive, neighter were quality gains after every epoch.

## Bipolar activation function
Then I tried using a bipolar activation function that assigns values in range [-1, 1].

![](bipolar_function.png)

This improved performance as the results were better than the reference solution after just 52 epochs (~40 minutes). I kept training this network for 200 epochs with results below. This took around 2 hours.

| Epoch no. | Epoch computation time | OK | Error |
| -- | -- | -- | -- |
|50|28.943273067474365|0.09608333333333334|0.1265|
|51|29.333019971847534|0.095483333333333337|0.12609999999999999|
|_52_|29.241729021072388|0.095000000000000001|0.12559999999999999|
|53|29.308738946914673|0.094633333333333333|0.12529999999999999|
|54|29.468106031417847|0.094149999999999998|0.12509999999999999|
|55|29.480031967163086|0.093600000000000003|0.125|
| ... | | | |
|100|28.769443035125732|0.076233333333333334|0.11849999999999999|
| ... | | | |
|110|28.691763877868652|0.073200000000000001|0.11840000000000001|
|111|28.780792951583862|0.072900000000000006|0.11799999999999999|
|112|28.636898994445801|0.072800000000000004|0.11749999999999999|
|113|28.781939029693604|0.072433333333333336|0.1174|
|114|28.562957048416138|0.072116666666666662|0.11749999999999999|
|115|28.61390209197998|0.071883333333333327|0.1177|
| ... | | | |
|150|28.857846021652222|0.064933333333333329|0.1162|
| ... | | | |
|200|28.924102067947388|0.056349999999999997|0.1162|

![](rep_bipolar_h100_e200_lr0001.png)

```matlab
cfmx =

   885     1    16    20     5     1    57     0    15     0     0
     6   965     4    16     5     0     1     0     3     0     0
    17     0   781    12   122     0    68     0     0     0     0
    35    10    14   881    38     0    18     0     3     1     0
     2     0    64    31   845     0    54     0     4     0     0
     0     0     1     1     0   948     0    26     3    21     0
   166     0    77    25    82     0   636     0    14     0     0
     0     0     0     0     0    15     0   959     0    26     0
     5     0     4     5     3     1     5     5   972     0     0
     0     0     0     1     0     4     1    28     0   966     0
```

I could see very little signs of overfitting where test error was going up while train error went down.

## Modifications
To increase learning performance and quality, I decided to modify training process by introducing:
1. data normalization,
1. adaptive learning rate,
1. inertia.

First, training was done for 20 epochs without any modification. Then, I introduced one at a time to finally use all of them at once and see how it affected learning process.

### 0. Unmodified training
With unmodified training run for 20 epochs, the error was ~13.95%

| Epoch no. | Epoch computation time | OK | Error |
| -- | -- | -- | -- |
|20|29.926826953887939|0.11956666666666667|0.13950000000000001|


### 1. Data normalization
I decided to normalize input data, so that mean value of samples is close to 0. Standard deviations should be in the same order of magnitude, so that none dominates over others. This is the case for this dataset.

The only thing to do then, was subtracting mean value from each sample values. The training results are largely the same as for unmodified training. This means that no significant improvement was made but also it didn't go the opposite way, so this modification can be used later.

| Epoch no. | Epoch computation time | OK | Error |
| -- | -- | -- | -- |
|20|32.035043954849243|0.11868333333333334|0.13880000000000001|

![](rep_bipolar_h100_e20_lr0001_normalized.png)

### 2. Adaptive learning rate
I decided to change learning rate from constant to adaptive. I decided to describe it with a formula: `lr = 0.01 / epoch` where `epoch` is the epoch number. This ensures that learning rate is always above 0 but will be very small after a significant number of epochs. The results were better than for the unmodified solution with almost no overfitting.

| Epoch no. | Epoch computation time | OK | Error |
| -- | -- | -- | -- |
|20|42.212798118591309|0.10871666666666667|0.13089999999999999|

![](rep_bipolar_h100_e20_adaptive_rate_always_above_0.png)

### 3. Inertia
During the learning proces there can be a situation when gradient is almost constant and it points to a local minimum. In that case a network with constant learning rate will make steps of a constant length. Introducing inertia will allow the presence of a constant gradient to increase the speed of weight modifications. The formula for changing weights would then be:

![](weight_mod_inertia.png)

Tests for `μ = 0.5` were performed. Results were better than for the unmodified solution but worse than when using adaptive learning rate.

| Epoch no. | Epoch computation time | OK | Error |
| -- | -- | -- | -- |
|20|40.89111590385437|0.11144999999999999|0.1326|

![](rep_bipolar_h100_e20_lr0001_inertia.png)

### 4. All modifications combined
The last experiment was to use all above-mentioned modifications together. This time the learning was run for 200 epochs. The results were better for the first ~120 epochs and then the improvement was not as fast as for the unmodified solution. This can likely be attributed to the imperfect formula for calculating the learning rate. It decreases with each epoch for modified solution while staying the same for unmodified one. Thus the plot for the modified solution flats out sooner and the results improve more slowly.

This training took around 2 hours.

| Epoch no. | Epoch computation time | OK | Error |
| -- | -- | -- | -- |
|20|57.763105869293213|0.09693333333333333|0.1235|
| ... | | | |
|50|58.296180009841919|0.087650000000000006|0.1187|
| ... | | | |
|100|57.652583122253418|0.081416666666666665|0.1176|
| ... | | | |
|110|57.28560209274292|0.080683333333333329|0.11749999999999999|
|111|57.553334951400757|0.08061666666666667|0.11749999999999999|
|112|56.602818965911865|0.08058333333333334|0.11749999999999999|
|113|56.358477830886841|0.080516666666666667|0.1176|
|114|56.163714170455933|0.080433333333333329|0.11749999999999999|
|115|56.087410926818848|0.08033333333333334|0.1176|
| ... | | | |
|150|58.953732967376709|0.07821666666666667|0.1171|
| ... | | | |
|195|68.086239814758301|0.076583333333333337|0.1169|
|196|70.87994909286499|0.076550000000000007|0.1169|
|197|75.024017810821533|0.076516666666666663|0.1169|
|198|72.107707023620605|0.076466666666666669|0.11700000000000001|
|199|71.155034065246582|0.076433333333333339|0.11700000000000001|
|200|56.036776065826416|0.076366666666666666|0.1169|

![](rep_bipolar_h100_e200_lrAdapt_all.png)

```matlab
cfmx =

   850     1    16    24     5     0    93     0    11     0     0
     5   962     3    23     3     0     3     0     1     0     0
    15     1   821    14    80     1    62     1     5     0     0
    25     7    15   891    35     0    24     0     3     0     0
     0     1    90    38   807     0    59     1     4     0     0
     0     0     0     1     0   941     0    34     2    22     0
   121     0    86    36    69     0   672     0    16     0     0
     0     0     0     0     0    19     0   957     0    24     0
     1     1     3     5     4     2     8     5   971     0     0
     1     0     0     0     0     9     0    31     0   959     0
```