function res = actdf(sfvalue)
% derivative of sigmoid activation function
% sfvalue - value of sigmoid activation function (!!!)

	% initial value given in the file
	% res = ones(size(sfvalue));

	% df for unipolar activation function
	res = sfvalue.*(1-sfvalue);
	% df for bipolar activation function
	% res = 1/2*(1-sfvalue.*sfvalue);
