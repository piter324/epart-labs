% mainscript is rather short this time

% primary component count
comp_count = 40; 

[tvec tlab tstv tstl] = readSets(); 

% let's look at the first digit in the training set
imshow(1-reshape(tvec(1,:), 28, 28)');

% let's check labels in both sets
[unique(tlab)'; unique(tstl)']

% compute and perform PCA transformation
[mu trmx] = prepTransform(tvec, comp_count);
tvec = pcaTransform(tvec, mu, trmx);
tstv = pcaTransform(tstv, mu, trmx);

% let's shift labels by one to use labels directly as indices
tlab += 1;
tstl += 1;

% To successfully prepare ensemble you have to implement perceptron function
% I would use 10 first zeros and 10 fisrt ones 
% and only 2 first primary components
% It'll allow printing of intermediate results in perceptron function

%
% YOUR CODE GOES HERE - testing of the perceptron function
% Remember: labels shifted by 1: 0 -> 1
ten_first_zeros_2features = tvec(tlab == 1, 1:2)(1:10,:);
ten_first_ones_2features = tvec(tlab == 2, 1:2)(1:10,:);

[sepplane fp fn] = perceptron(ten_first_ones_2features, ten_first_zeros_2features)

% test the perceptron on all features for the first 10 zeros and ones
ten_first_zeros_allfeatures = tvec(tlab == 1, 1:end)(1:10,:);
ten_first_ones_allfeatures = tvec(tlab == 2, 1:end)(1:10,:);

[sepplane fp fn] = perceptron(ten_first_ones_allfeatures, ten_first_zeros_allfeatures)

% Now experiment with the margin 
% It make sense to use "easy" (0 vs. 1) and "difficult" (4 vs. 9) cases.
%
% YOUR CODE GOES HERE - experiments with marging in the perceptron function

all_zeros_allfeatures = tvec(tlab == 1, 1:end);
all_ones_allfeatures = tvec(tlab == 2, 1:end);
all_fours_allfeatures = tvec(tlab == 5, 1:end);
all_nines_allfeatures = tvec(tlab == 10, 1:end);

res01 = []
res49 = []
margins = [0 0.01 0.1 0.5 1]
for idx = 1:columns(margins)
    marg = margins(idx);
    ["Current margin:" num2str(marg)]

    [sepplane fp fn] = perceptron(all_zeros_allfeatures, all_ones_allfeatures, marg);
    % false posities | false negatives | summed up | percentage of falses in the whole set
    res01(idx,:) = [marg fp fn fp+fn (fp+fn)/(rows(all_zeros_allfeatures)+rows(all_ones_allfeatures))];

    [sepplane fp fn] = perceptron(all_fours_allfeatures, all_nines_allfeatures, marg);
    res49(idx,:) = [marg fp fn fp+fn (fp+fn)/(rows(all_fours_allfeatures)+rows(all_nines_allfeatures))];
endfor

% training of the whole ensemble
[ovo, ovo_erates] = trainOVOensamble(tvec, tlab, @perceptron);

% check your ensemble on train set
clab = unamvoting(tvec, ovo);
cfmx = confMx(tlab, clab)
compErrors(cfmx)

% repeat on test set
clab = unamvoting(tstv, ovo);
cfmx = confMx(tstl, clab)
compErrors(cfmx)

% ONE vs. REST
% training of the whole ensemble
[ovr, ovr_erates] = trainOVRensamble(tvec, tlab, @perceptron);

% check your ensemble on train set
clabOVR = unamvotingOVR(tvec, ovr);
cfmxOVR = confMx(tlab, clabOVR)
compErrors(cfmxOVR)

% repeat on test set
clabOVR = unamvotingOVR(tstv, ovr);
cfmxOVR = confMx(tstl, clabOVR)
compErrors(cfmxOVR)

%% ---- EXPANDING FEATURES ----
exp_tvec = expandFeatures(tvec);
exp_tstv = expandFeatures(tstv);

% checking margins influence
all_zeros_allfeatures = exp_tvec(tlab == 1, 1:end);
all_ones_allfeatures = exp_tvec(tlab == 2, 1:end);
all_fours_allfeatures = exp_tvec(tlab == 5, 1:end);
all_nines_allfeatures = exp_tvec(tlab == 10, 1:end);

res01 = []
res49 = []
margins = [0 0.01 0.1 0.5 1]
for idx = 1:columns(margins)
    marg = margins(idx);
    ["Current margin:" num2str(marg)]

    [sepplane fp fn] = perceptron(all_zeros_allfeatures, all_ones_allfeatures, marg);
    % false posities | false negatives | summed up | percentage of falses in the whole set
    res01(idx,:) = [marg fp fn fp+fn (fp+fn)/(rows(all_zeros_allfeatures)+rows(all_ones_allfeatures))];

    [sepplane fp fn] = perceptron(all_fours_allfeatures, all_nines_allfeatures, marg);
    res49(idx,:) = [marg fp fn fp+fn (fp+fn)/(rows(all_fours_allfeatures)+rows(all_nines_allfeatures))];
endfor

% ONE vs.ONE
% training of the whole ensemble
[ovo, ovo_erates] = trainOVOensamble(exp_tvec, tlab, @perceptron);

% check your ensemble on train set
clab = unamvoting(exp_tvec, ovo);
cfmx = confMx(tlab, clab)
compErrors(cfmx)

% repeat on test set
clab = unamvoting(exp_tstv, ovo);
cfmx = confMx(tstl, clab)
compErrors(cfmx)

% ONE vs. REST
% training of the whole ensemble
[ovr, ovr_erates] = trainOVRensamble(exp_tvec, tlab, @perceptron);

% check your ensemble on train set
clabOVR = unamvotingOVR(exp_tvec, ovr);
cfmxOVR = confMx(tlab, clabOVR)
compErrors(cfmxOVR)

% repeat on test set
clabOVR = unamvotingOVR(exp_tstv, ovr);
cfmxOVR = confMx(tstl, clabOVR)
compErrors(cfmxOVR)

% ---- SVM ----

% train svm on train set
model = svmtrain(tlab, tvec);

% predict classes on test set
[predicted_labels, accuracy, sepplane] = svmpredict(tstl, tstv, model);

% SVM on expanded features
model = svmtrain(tlab, exp_tvec);
[predicted_labels, accuracy, sepplane] = svmpredict(tstl, exp_tstv, model);