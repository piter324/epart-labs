function [sepplane fp fn] = perceptron(pclass, nclass, margin=0)
% Computes separating plane (linear classifier) using
% perceptron method.
% pclass - 'positive' class (one row contains one sample)
% nclass - 'negative' class (one row contains one sample)
% Output:
% sepplane - row vector of separating plane coefficients
% fp - false positive count (i.e. number of misclassified samples of pclass)
% fn - false negative count (i.e. number of misclassified samples of nclass)

  sepplane = rand(1, columns(pclass) + 1) - 0.5;
  tset = [ ones(rows(pclass), 1) pclass; -ones(rows(nclass), 1) -nclass];
  nPos = rows(pclass); % number of positive samples
  nNeg = rows(nclass); % number of negative samples
  min_change = 0.001;

  error = 0;
  i = 1;
  
  %% 1. Check which samples are misclassified (boolean column vector)
  res = tset * sepplane';  
  misclassified = res<margin;
  
  do
    alpha = min_change/i;
    %%% YOUR CODE GOES HERE %%%
    %% You should:
    %% 2. Compute separating plane correction 
    %%		This is sum of misclassified samples coordinate times learning rate
    error = sum(tset(misclassified, :));
    d_sepplane = alpha * error;

    %% 3. Modify solution (i.e. sepplane)
    sepplane += d_sepplane;

    %% 1. Check which samples are misclassified (boolean column vector)
    %% It's here because of the additional stop criterion,
    %% no need to calculate it here and then again at the beginning of the loop
    res = tset * sepplane';
    misclassified = res<margin;

    %% 4. Optionally you can include additional conditions to the stop criterion
    %%		200 iterations can take a while and probably in most cases is unnecessary
    %% If there are no false positives and false negatives, then the loop should be stopped
    if (sum(res<margin) == 0)
      break;
    endif

    error = 0;
    ++i;
  until i > 200;

  %%% YOUR CODE GOES HERE %%%
  %% You should:
  %% 1. Compute the numbers of false positives and false negatives

  fp = sum(res(1:nPos, 1) < 0);
  fn = sum(res(nPos + 1 : nPos + nNeg, 1) < 0);
