function [ovrsp errors] = trainOVRensamble(tset, tlab, htrain)
% Trains a set of linear classifiers (one versus rest class)
% on the training set using trainSelect function
% tset - training set samples
% tlab - labels of the samples in the training set
% htrain - handle to proper function computing separating plane
% ovrsp - one versus rest class linear classifiers matrix
%   the first column contains positive class label
%   the second column contains negative class label
%   columns (2:end) contain separating plane coefficients

  % number of unique labels is number of ovr classifiers
  labels = unique(tlab);
  
  % we have as much rows as unique labes
  % and columns for label bias and weights
  ovrsp = zeros(rows(labels), 1 + 1 + columns(tset));
  
  % in columns: label, fp, np
  errors = zeros(rows(labels), 3);
   
  for i=1:rows(labels)
  
	% store label in the first column
    ovrsp(i, 1) = i;
	errors(i, 1) = i;
	
	% select samples of two digits from the training set
    posSamples = tset(tlab == labels(i), :);
    negSamples = tset(tlab != labels(i), :);
	
	% train 5 classifiers and select the best one
    [sp fp fn] = trainSelectOVR(posSamples, negSamples, 5, htrain);
	
	errors(i, 2:5) = [fp fn fp+fn (fp+fn)/(rows(posSamples)+rows(negSamples))];
	
    % store the separating plane coefficients (this is our classifier)
	% in ovr matrix
    ovrsp(i, 2:end) = sp;
  end
