# EPART laboratory report
Session 3&4: Recognition of handwritten digits with linear classifiers

## Intro
In this assignment, my task was to recognize handwritten digits. I used use one of the most widely used data sets in the pattern recognition - MNIST handwritten digits (http://yann.lecun.com/exdb/mnist). Images were normalized, which in this case means that each was scaled to dimensions of 20x20 pixels, converted to grayscale and put in the center of 28x28 pixels image according to its center of gravity.

My task was to produce a classifier that used linear classifiers distinguishing individual digits. In addition to the quality of classification on the test set I also produced confusion matrix.
Additional task was to propose a different, better method for determining the classifier decision than simply voting of elementary classifiers. In my case the solution was using Support Vector Machine.

The point of reference was the classic voting (45 linear ovo, i.e. one versus one, classifiers) classifying pairs of digits if it collected maximum possible number of votes = 9. In other cases, voting classifier made reject decision. The tests used the first 40 principal components. Classification results were summarized in the table below.
![](reference_table.png)

## Preparing and testing perceptron function
The first task was to create a perceptron function which would serve as a classifier. Such classifier is able to distinguish between "positive" samples and "negative" samples by creating a separating plane with the number of dimensions equal to the number of sample features used in classification. That separating plane has two sides - the classification outcome depends on the side a sample falls on.

My perceptron function iterates over samples and adjusts separating plane coefficients up to 200 times. However, if the number of misclassified samples (false positives and false negatives) is equal to zero, further adjustments are are not necessary, so the loop stops and returns coefficients calculated up to that point as the final separating plane.

To test a perceptron function, I decided to extract 10 first "zero" digits and 10 first "one" digits since they are quite easily distinguishable from each other due to not having too many strokes in common, unlike digits like "four" and "nine" or "seven" and "one", for example.

The results of this classification, in just 2 dimensions at first, so taking only 2 features, are below:
```matlab
sepplane =

   0.348642  -0.097588  -0.129348

fp = 0
fn =  1
```
The error rate for this really limited dataset is 1/20 or 5%.

When 40 dimensions are taken into account, results are not very different:
```matlab
sepplane =

 Columns 1 through 12:

   0.366062  -0.345284  -0.409874   0.156655   0.042703   0.182656   0.320347   0.359474   0.051320  -0.275890   0.239739   0.387676

 Columns 13 through 24:

  -0.312243   0.163212  -0.503695   0.298782  -0.392850   0.113275  -0.330724   0.044552   0.374375   0.301174  -0.445903   0.181990

 Columns 25 through 36:

  -0.396009   0.475156  -0.183533   0.142477   0.255826   0.019524  -0.493713  -0.279755   0.150481   0.222741  -0.205793   0.392221

 Columns 37 through 41:

   0.266304   0.082618  -0.226834   0.267618  -0.389362

fp =  2
fn = 0
```
We need to keep in mind that here, the number of samples plays a crucial role in the classifier quality.

## Using all samples and changing margins
Then there was time to try using all samples, not just 10 first ones, and to add more diversity to the mix by comparing results for "one" and "zero" with those for "four" and "nine", which have a lot more strokes in common. By default, perceptron functions considers a sample misclassified when the dot product of a separating hyperplane is lower than zero. So to make these experiments even more interesting, I decided to change this margin value and see how it affects classification quality.

Tested margin values were:
```matlab
margins = [0 0.01 0.1 0.5 1]
```

The first column below represents the value of a margin, then the number of false positives, then false negatives, then the sum of these and, finally, the share of misclassified samples in the whole set. The results for classifying "one" versus "zero" are:
```matlab
res01 =

    0.00000   58.00000   24.00000   82.00000    0.00647
    0.01000   48.00000   10.00000   58.00000    0.00458
    0.10000   51.00000   13.00000   64.00000    0.00505
    0.50000   58.00000    9.00000   67.00000    0.00529
    1.00000   60.00000   12.00000   72.00000    0.00568
```

And for "four" vs "nine":
```matlab
res49 =

     0.00000   266.00000   330.00000   596.00000     0.05055
     0.01000   275.00000   339.00000   614.00000     0.05207
     0.10000   283.00000   364.00000   647.00000     0.05487
     0.50000   262.00000   332.00000   594.00000     0.05038
     1.00000   243.00000   327.00000   570.00000     0.04834
```

Misclassification of four and nine happens a lot more often but that is expected due to them having more strokes in common.

It seems that changing the margin value doesn't change results significantly, so I decided to stick to a margin equal to 0.

## One vs One classifier ensemble
Then it was time to prepare the ensemble of one vs. one classifiers. In order for a sample to be classified, all classifiers must agree on the outcome, meaning that if there's no unanimity between them, a sample has to be rejected.

After training said ensemble, the number of false positives and negatives was as presented below. First and second columns represent labels of samples to distinguish from each other, then there is the number of false positives and false negatives, then the summed up number of misclassified samples and then a share of misclassified samples in all positive and negative samples:
```matlab
ovo_erates =

     1     2    27    13    40     0.0031583
     1     3    92   119   211     0.0177594
     1     4    61   119   180     0.0149328
     1     5    65    31    96     0.0081598
     1     6   152   249   401     0.0353491
     1     7   192   153   345     0.0291361
     1     8    44    38    82     0.0067279
     1     9    93   138   231     0.0196195
     1    10    74    78   152     0.0128032
     2     3    91   134   225     0.0177165
     2     4    66   362   428     0.0332479
     2     5    57    62   119     0.0094565
     2     6    80   105   185     0.0152101
     2     7    37    53    90     0.0071090
     2     8    54   106   160     0.0123011
     2     9   179   256   435     0.0345430
     2    10    43    76   119     0.0093767
     3     4   194   235   429     0.0354868
     3     5   152   142   294     0.0249153
     3     6   180   173   353     0.0310221
     3     7   272   232   504     0.0424385
     3     8   250   128   378     0.0309253
     3     9   219   231   450     0.0381065
     3    10   198   109   307     0.0257832
     4     5   201    42   243     0.0202957
     4     6   376   331   707     0.0612015
     4     7   186    58   244     0.0202506
     4     8   231   103   334     0.0269442
     4     9   273   277   550     0.0459022
     4    10   163   176   339     0.0280629
     5     6    88    98   186     0.0165143
     5     7   112    85   197     0.0167517
     5     8   100   191   291     0.0240357
     5     9    74   219   293     0.0250577
     5    10   257   306   563     0.0477483
     6     7   271   169   440     0.0388041
     6     8   121    76   197     0.0168578
     6     9   293   354   647     0.0573989
     6    10   129   163   292     0.0256816
     7     8     8    53    61     0.0050070
     7     9    69   246   315     0.0267652
     7    10    28    77   105     0.0088481
     8     9    78   130   208     0.0171674
     8    10   360   381   741     0.0606681
     9    10   245   198   443     0.0375424
```

A confusion matrix for classifying samples from the training set is presented below. Values placed on the diagonal axis represent the number of samples classified correctly, last column represents the number of rejected samples and the rest represent misclassified samples:
```matlab
cfmx =

   5467      0     35     11     10     80    106     11     53     14    136
      0   6387     60     12      7     25      8     14    113      8    108
     35     27   5217     48     66     18    126    109    104     33    175
     18     70     92   5150      1    228     46     89    111     39    287
      6     21     63      0   5295      6     53     19     45    216    118
     52     36     35    179     32   4414    114     33    125     50    351
     47     23    136      3     31     92   5410      1     39      0    136
     10     50     74     13     60     10      3   5699     15    183    148
     18    113     81    111     32    122     48     26   4927     94    279
     14     26     47     63    211     31      4    250     70   5098    135
```
We can see that the majority of samples have been classified correctly. After running `compErrors` function to analyze the confusion matrix further, we can see that ~88.44% samples were classified correctly, ~8.43% were misclassified and ~3.12% were rejected:
```matlab
ans =

   0.884400   0.084383   0.031217
```

A confusion matrix for classifying samples from the testing set is presented below:
```matlab
cfmx =

    902      0      5      0      0     14     21      1      5      1     31
      0   1079      5      5      1      1      3      1     25      0     15
      9      1    908     11     13      3     20     18     23      4     22
      3      3     13    880      0     25      5     17     16      4     44
      1      0      4      0    894      1     13      3      5     33     28
      8      6      5     35      6    744     18      6     17      8     39
     18      3     10      1      7     14    885      0      4      0     16
      0      8     20      2      8      0      0    925      5     35     25
      5      7     10     16     11     22     15      7    829     10     42
      6      6      6      6     43      2      1     25      8    871     35
```
This time ~89.17% of samples were classified correctly, ~7.86% were misclassified and ~2.97% were rejected:
```matlab
ans =

   0.891700   0.078600   0.029700
```

## One vs. Rest classifiers
This time we only have 10 classifiers since samples of each label have to be classified against all others. Error rates for each classifier are shown below. First columns is a label, then the number of false positives, false negatives, overall number of misclassified samples and a share of misses in the whole set of positive and negative samples.
```matlab
ovr_erates =

      1.000000    604.000000    379.000000    983.000000      0.016383
      2.000000    498.000000    451.000000    949.000000      0.015817
      3.000000   1150.000000    665.000000   1815.000000      0.030250
      4.000000   1331.000000    707.000000   2038.000000      0.033967
      5.000000    978.000000    681.000000   1659.000000      0.027650
      6.000000   1911.000000   1193.000000   3104.000000      0.051733
      7.000000    708.000000    518.000000   1226.000000      0.020433
      8.000000    853.000000    608.000000   1461.000000      0.024350
      9.000000   2021.000000    933.000000   2954.000000      0.049233
     10.000000   1671.000000   1053.000000   2724.000000      0.045400
```

The confusion matrix for classifying samples from the training set using OVR classifier ensemble:
```matlab
cfmxOVR =

   5319      1      9      4      4     20     35      4      4      1    522
      0   6244     40      9      3     24     10     14     63      2    333
     82     71   4717     49     77     15     69     61     80     18    719
     33     18    151   4737      5    127     42     63     59     29    867
     19     39      9      7   4851     11     33     10     24    123    716
     33     35     32    284     55   3320     83     15     43     21   1500
     82      5    149      2     65    110   4948      0     20      0    537
     91     71    123     55     87     28      3   5202      7     57    541
     18    163     47     88     16    160     19      9   3639     22   1670
     21     47     27     81    340     73      1    244     36   3866   1213
```
Which gives us ~78.07% correctly classified samples, ~7.57% of misclassified samples and ~14.36% of rejected samples.
```matlab
ans =

   0.780717   0.075650   0.143633
```

Then the confusion matrix for the training set:
```matlab
cfmxOVR =

    893      0      1      2      0      5      4      1      0      0     74
      0   1050      0      1      1      1      3      1     18      0     60
     21     14    818      8     13      2      9     13     20      2    112
      5      1     14    807      0     16      3     11     10      3    140
      2      5      2      2    805      4      7      3      2     30    120
      6      5      5     41     11    573     11      3      5      2    230
     21      0     15      1     18     20    802      0      1      0     80
      7     14     36      9      9      2      1    861      1     12     76
      7      8      5     14      5     31      7      5    644      0    248
      4      9      5      7     70     21      0     33      5    672    183
```
Which gives us ~79.25% correctly classified samples, ~7.52% of misclassified samples and ~13.23% of rejected samples.
```matlab
ans =

   0.792500   0.075200   0.132300
```
We can see that OVR classifier ensemble and OVO classifier ensemble had similar percentage of incorrect classification outcomes but, at the same time, OVR had more rejections and, consequently, less samples classified correctly. The conclusion is that OVO ensemble performs better at classifying our samples. Higher rejection rate has to do with positive and negative sample sets being imbalanced, which makes it harder to achieve unanimity among classifiers.

## Expanding features
After classifying samples using OVO and OVR enembles, I went on to check how expanding the number of features would affect classification quality. I used `expandFeatures` function to get 780 additional features from the initial 40. That gave 860 features in total, which I used to train and test new ensembles of OVO and OVR classifiers.

## OVO classifier for expanded features
Error rates for each OVO classifier trained on the expanded feature set are presented below. Columns have the same meaning as last time.
```matlab
ovo_erates =

     1     2    21    29    50     0.0039479
     1     3    96    57   153     0.0128777
     1     4    84    72   156     0.0129418
     1     5    45   166   211     0.0179346
     1     6   118   158   276     0.0243300
     1     7   147   163   310     0.0261802
     1     8    82    90   172     0.0141122
     1     9   114   209   323     0.0274333
     1    10    55    99   154     0.0129717
     2     3    86    84   170     0.0133858
     2     4    82    71   153     0.0118853
     2     5   101   101   202     0.0160521
     2     6    56    55   111     0.0091260
     2     7    55    46   101     0.0079779
     2     8    79    55   134     0.0103021
     2     9   115   174   289     0.0229493
     2    10    54    71   125     0.0098495
     3     4   146   145   291     0.0240715
     3     5    70    67   137     0.0116102
     3     6    41    76   117     0.0102821
     3     7    35    56    91     0.0076625
     3     8    65    77   142     0.0116174
     3     9   115   125   240     0.0203235
     3    10    64    55   119     0.0099941
     4     5    46    34    80     0.0066817
     4     6   150   136   286     0.0247576
     4     7    64    39   103     0.0085484
     4     8    72    67   139     0.0112133
     4     9   157   178   335     0.0279586
     4    10   130   142   272     0.0225166
     5     6    44    34    78     0.0069253
     5     7    56    57   113     0.0096088
     5     8    65    85   150     0.0123895
     5     9    59    69   128     0.0109467
     5    10   138   154   292     0.0247647
     6     7   131   104   235     0.0207249
     6     8    40    36    76     0.0065035
     6     9   100   131   231     0.0204933
     6    10    93    90   183     0.0160950
     7     8    13    27    40     0.0032833
     7     9    78    71   149     0.0126604
     7    10    25    26    51     0.0042976
     8     9    51    74   125     0.0103169
     8    10   182   204   386     0.0316031
     9    10   164   125   289     0.0244915
```
Error rates for OVO classifiers trained on expanded feature set are generally lower that for those trained on the original feature set, which is a good omen for the classification results.

The confusion matrix for training data:
```matlab
cfmx =

   5586      4     35     23      3     31     92      2     43      7     97
      3   6527     42     13     24      0      3     11     42      7     70
     33     43   5539     69     31      4     18     47     56      7    111
     13     31     83   5596      6     79     19     40     85     53    126
     46     42     24      6   5400      3     36     18     20    119    128
     70     23     13    100     13   4869     65      8     50     50    160
    108     36     16      9     21     46   5582      0     20      2     78
     33     50     36     15     33      2      0   5882     12    120     82
     60    117     45    113     16     79     26     19   5116     94    166
     47     30     14     72     84     18      5    162     41   5357    119
```

Which gives ~92.42% samples classified correctly, ~5.68% incorrectly and ~1.9% samples rejected. Thie means that results for the expanded feature set were **better** than for the original one since the result for it was ~88.44% samples classified correctly.
```matlab
ans =

   0.924233   0.056817   0.018950
```

The confusion matrix for testing set:
```matlab
cfmx =

    934      0      3      4      0      6     14      1      3      0     15
      1   1109      3      1      3      0      2      0      6      0     10
     10      8    945     12      8      0      5     11     12      2     19
      2      1      6    941      0     15      1     11     14      3     16
     16      1      6      4    892      2     10      1      2     25     23
     10      2      9     18      3    794      7      2     13      8     26
     33      3      4      1      6      5    892      0      8      0      6
      2      9     12      4      3      0      0    962      5     14     17
     19      4      7     24      3     14      0      6    866      5     26
     15      5      3     11     17      2      1     23      3    906     23
```

Which gives ~92.41% samples classified correctly, ~5.78% incorrectly and ~1.81% samples rejected. This is also better than for the original feature set with the result for it being ~89.17% classified correctly.
```matlab
ans =

   0.924100   0.057800   0.018100
```

This classifer ensemble yielded the best results so far, also surpassing the reference ensemble mentioned in the [Intro](#intro).

## OVR classifier for expanded features
Error rates for each OVR classifier trained on the expanded feature set are presented below. Columns have the same meaning as last time.
```matlab
ovr_erates =

      1.0000000    466.0000000    228.0000000    694.0000000      0.0115667
      2.0000000    407.0000000    180.0000000    587.0000000      0.0097833
      3.0000000    734.0000000    374.0000000   1108.0000000      0.0184667
      4.0000000    898.0000000    523.0000000   1421.0000000      0.0236833
      5.0000000   1279.0000000    309.0000000   1588.0000000      0.0264667
      6.0000000    734.0000000    490.0000000   1224.0000000      0.0204000
      7.0000000    446.0000000    234.0000000    680.0000000      0.0113333
      8.0000000   1155.0000000    180.0000000   1335.0000000      0.0222500
      9.0000000   1971.0000000    314.0000000   2285.0000000      0.0380833
     10.0000000    947.0000000    664.0000000   1611.0000000      0.0268500
```
Error rates for OVR classifiers trained on expanded feature set are generally lower that for those trained on the original feature set, same as for OVO classifiers, which is a good omen for the classification results.

The confusion matrix for training data:
```matlab
cfmxOVR =

   5457      0     12      3      0     10     40      0      4      0    397
      0   6335     22      5      0      0      2      2      4      1    371
     24     14   5218     26     14      6      5     22     21      2    606
      5      4     86   5197      0     64      7     10     38     16    704
     10      9     25      1   4554      0     34      7      7    184   1011
     29     11     27    185     10   4497     25      0      2     10    625
    128     19     42      4     37     43   5323      0      0      0    322
      4     54     78      1     16      5      0   5067      6     86    948
     13     61     54    193     12    149     11      3   3678     41   1636
     15      8     19     57    187      9      4    117     19   4770    744
```

Which gives ~83.49% samples classified correctly, ~4.23% incorrectly and ~12.27% samples rejected. Thie means that results for the expanded feature set were **better** than for the original one since the result for it was ~78.07% samples classified correctly.
```matlab
ans =

   0.834933   0.042333   0.122733
```

The confusion matrix for testing set: 
```matlab
cfmxOVR =

    907      0      2      0      0      2      3      1      0      0     65
      0   1081      3      1      1      0      1      0      0      0     48
     12      0    891      3      6      1      3      8      7      0    101
      0      0      9    881      0     10      0      5      9      2     94
      3      0      5      0    770      0      8      0      0     30    166
      3      1      8     34      0    726      5      0      1      2    112
     33      3      5      0      9      9    858      0      1      0     40
      1     10     18      0      1      0      0    830      2     16    150
      3      0     12     30      3     23      0      2    634      6    261
      2      4      6      5     50      0      2      6      1    820    113
```

Which gives ~83.98% samples classified correctly, ~4.52% incorrectly and ~11.5% samples rejected. This is also better than for the original feature set with the result for it being ~79.25% classified correctly.
```matlab
ans =

   0.839800   0.045200   0.115000
```

## Support Vector Machine
In order to check how ensembles described above stack up against other methods of linear classification, I decided to try Support Vector Machine and compare its performance to the best results achieved so far. I used `svmtrain` function available as a part of `Libsvm` open source machine learning library developed at National Taiwan University by Chih-Chung Chang and Chih-Jen Lin. It is described in more details along with download links here: https://medium.com/@jalalmansoori/how-to-use-libsvm-in-octave-32009301aa0. I downloaded it, unpacked and ran `make` in the `matlab` directory to get functions ready to use with Octave in the form of `svmpredict.mex` and `svmtrain.mex`. I then put these files into the project directory and ran them as functions to get results below.

Results for the original testing set:
```matlab
> [predicted_labels, accuracy, sepplane] = svmpredict(tstl, tstv, model);
Accuracy = 98.21% (9821/10000) (classification)
```

Results for the expanded testing set:
```matlab
> [predicted_labels, accuracy, sepplane] = svmpredict(tstl, exp_tstv, model);
Accuracy = 98.39% (9839/10000) (classification)
```

Classification done using SVM yields the best results of all classifiers mentioned in this document with accuracy over 98%. To recall, the best classifier ensemble, which was OVO trained on the expanded feature set, achieved accuracy of only a little over 92%.