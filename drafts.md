For this step, I need a priori probability to be `0.125` for each classifier, so I need to make sure that the number of samples in the training set is equal for each of 8 classes. Right now, after removing outliers, class 3 has 2 fewer representants than the rest:
```matlab
> [labels'; sum(train(:,1) == labels')]
ans =

     1     2     3     4     5     6     7     8
   228   228   226   228   228   228   228   228
```
I've decided to remove first 2 samples from other classes, to bring a priori probability to `0.125`:
```matlab
> [labels'; sum(train(:,1) == labels')]
ans =

     1     2     3     4     5     6     7     8
   226   226   226   226   226   226   226   226
```

CODE:
```matlab
% remove two first samples from classes !=3 to bring a priori probability to 0.125
labels = unique(train(:,1))
[labels'; sum(train(:,1) == labels')]

new_train = []
for label_ind = 1:rows(labels)
	class_samples = train(train(:,1) == labels(label_ind), :);
	if (labels(label_ind) != 3)
		new_train = [new_train; class_samples(3:rows(class_samples), :)];
	else
		new_train = [new_train; class_samples];
	endif
endfor
[labels'; sum(new_train(:,1) == labels')]
train = new_train;
```
---