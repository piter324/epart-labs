function errcf = jackknife(ts)
% performs leave-one-out test of cls1nn classifier on ts
% ts - training set; each row represents one sample
%       first column contains sample label
% errcf - error coefficient of cls1nn on ts
    cres = zeros(rows(ts), 1)
    for i = 1:rows(ts)
        cres(i) = cls1nn(ts([1:i-1,i+1:end],:), ts(i,2:end));
    end
    errcf = mean(ts(:,1) != cres)
end