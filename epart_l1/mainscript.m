load iris2.txt
iris2(:,1) = 2;
load iris3.txt
iris3(:,1) = 3;
% to make sure the first value was changed properly
mean(iris3)
min(iris3)
% to join one under the other (;)
iris = [iris2 ; iris3];

% prepare the classifier
ts = iris(47:54,:)
% choose x from iris as a sample - without label according to function specs
% makes sense since the distance to one of neighbours will be 0
x = ts(6,2:end)
% Euclidean distance
% step 1
ts(:,2:end) - x
% for 6th row it will give all 0.00 as expected
% step 2 - square differences
(ts(:,2:end) - x) .^ 2
% step 3 - sum up square differences for each sample - sum each row
% 6th row gives 0.00 again as expected
sum( (ts(:,2:end) - x) .^ 2, 2)
% step 4 - sum squares with a function
sumsq( ts(:,2:end) - x, 2)

% testing cls1nn
for i=1:rows(ts)
    cls1nn(ts, ts(i, 2:end));
end