function lab = cls1nn(ts,x)
% 1-NN classifier - one nearest neighbour - using Euclidean distance
% ts - training set; each row represents one sample
%       first column contains sample label
% x - sample to be classified; no label here
% lab - the label of x's nearest neighbour in ts
    sqdist = sumsq( ts(:,2:end) - x, 2);
    [v iv] = min(sqdist);
    lab = ts(iv, 1);
end